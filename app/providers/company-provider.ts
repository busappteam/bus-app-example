import {Injectable} from 'angular2/core';
import {Http, RequestOptionsArgs, Headers, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx'; //import all the Observable operators, such as map
import {AppSetting} from '../app-setting';

/*
  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CompanyProvider {
  constructor(public http: Http, public appSetting: AppSetting) { }

  data: any = null;

  private handleError(error: any) {
    console.log("in handleError 1111111");
    console.dir(error);
    console.log("in handleError 22222222222");
    // In a real world app, we might send the error to remote logging infrastructure
    let errMsg = error._body;
    console.error('in handleError' + errMsg); 
    return Observable.throw(errMsg);
  }

  addCompany(cid: string) {
    console.log('CompanyProvider:addCompany' + cid );

    var userOptions: any = {'cid': cid};

    var postOptions: RequestOptionsArgs = {
      headers: new Headers({ 'Content-Type': 'application/json' })
    };

    console.log('CompanyProvider:addCompany' + this.appSetting.webApiServer);

    // return an observable
    return this.http.post(this.appSetting.webApiServer + '/api/company', 
                          JSON.stringify(userOptions), postOptions)
                    .map((res: Response) => {console.dir(res); console.log("ssssssssssss"); return res.json()})
                    .catch(this.handleError)
  }
}