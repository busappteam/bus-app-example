import {Page, NavController} from 'ionic-angular';
import {Input} from 'angular2/core';
import {CompanyProvider} from '../../providers/company-provider';

/*
  Generated class for the CompanyPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/company/company.html',
  providers: [CompanyProvider]
})
export class CompanyPage {
  constructor(public nav: NavController, public company: CompanyProvider) { }
  result: string = "result";

  @Input() cid: any;

  addCompany() {
    this.company.addCompany(this.cid)
        .subscribe(res => {
                      // we've got back the response data in json
                      console.dir(res);
                      console.log('CompanyPage: addCompany got back data: ' + JSON.stringify(res));
                      this.result = "success";
                      //this.nav.push(DriverConsolePage);
                    },
                    err => {
                      this.result = err;
                      console.dir(err);
                      console.log('error in addCompany. ' + this.result );
                    }
         )
  }
}

 