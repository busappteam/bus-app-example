import {Page, NavController} from 'ionic-angular';
import {Input} from 'angular2/core';
import {AuthProvider} from '../../providers/auth-provider';
import {DriverConsolePage} from '../driver-console/driver-console';

/*
  Generated class for the SignUpPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/sign-up/sign-up.html',
  providers: [AuthProvider]
})
export class SignUpPage {
  constructor(public nav: NavController, public auth: AuthProvider) {}
  result: string = "result";

  @Input() username: any;
  @Input() password: any;

  signUp() {
    this.auth.signUp(this.username, this.password)
        .subscribe(res => {
                      // we've got back the response data in json
                      console.dir(res);
                      console.log('SignUpPage: signUp got back data: ' + JSON.stringify(res));
                      this.result = "success";
                      //this.nav.push(DriverConsolePage);
                    },
                    err => {
                      this.result = err.message;
                      console.log('error in singing up. ' + this.result );
                      console.log('error in singing up. ' + JSON.stringify(err));
                    }
         )
  }
}
