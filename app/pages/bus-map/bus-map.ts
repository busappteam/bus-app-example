import {Page, NavController} from 'ionic-angular';
import {BaiduMapUtils} from './baidu-map-utils';
import {Component, SimpleChange, Input, OnInit, OnChanges, ChangeDetectionStrategy, ElementRef} from 'angular2/core';
import {convertWGS84ToBD09} from '../../util/mapUtil';
import {Http, RequestOptionsArgs, Headers} from 'angular2/http';
import {AppSetting} from '../../app-setting';

/*
  Generated class for the BusMapPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
    templateUrl: 'build/pages/bus-map/bus-map.html',
  	directives:[BaiduMapUtils]
})
export class BusMapPage {
	win: any = window;
	mapUtils: BaiduMapUtils;
	you:any;
	locationInterval:any;

    constructor(public nav: NavController, public http: Http, public appSetting: AppSetting) {}

    ngOnInit() {

		this.mapUtils = new BaiduMapUtils(document.getElementById('map-container'));

	  	this.mapUtils.mapKey = "fDUEKjmQHlSmxNFvznHSBLGvYRNzp7E5";
        this.mapUtils.options = {
            center: {
                longitude: 113.5771190191858,
                latitude: 22.25800282442914
            },
            zoom: 17,
            markers: [{
                longitude: 113.5771190191858,
                latitude: 31.245554,
                title: 'Where',
                content: 'Put description here'
            }]
        };

		this.mapUtils.drawBaiduMap();

        var baiduMap = this.win.baiduMap;
        if (baiduMap && baiduMap.status === 'loading') {
            baiduMap.callbacks.push(() => {
                this.location();
                this.drawBusRoute();
                this.getBusRoute();
            });
            return;
        }

        if (baiduMap && baiduMap.status === 'loaded') {
        	this.location();
        	this.drawBusRoute();
        	this.getBusRoute();
        }
    }

    location(){
    	var self = this;
    	var BMap = this.win.BMap;
        var map = this.mapUtils.map;

		this.locationInterval = setInterval(() => {
	      	if (navigator.geolocation){
	            var options = { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true };
	            navigator.geolocation.getCurrentPosition(function (position){
	                var wgs84Point = new BMap.Point(position.coords.longitude, position.coords.latitude);
	                console.log(wgs84Point);
	                var bd09Point = convertWGS84ToBD09(wgs84Point.lng, wgs84Point.lat);
	                console.log(bd09Point);
	                var point = new BMap.Point(bd09Point.lng, bd09Point.lat);
	                if(self.you){
	                	self.you.setPosition(point);

	                	var bounds = map.getBounds();
	                	// if current point is not visiable, then move map to let current point be visiable
	                	if(!bounds.containsPoint(point)){
	                		var extendBounds = bounds.extend(point);
	                		var outSideSouthWestPoint = extendBounds.getSouthWest();
	                		var inSideSouthWestPoint = bounds.getSouthWest();

	                		var center = bounds.getCenter();
	                		var deltaLng = outSideSouthWestPoint.lng - inSideSouthWestPoint.lng;
	                		var deltaLat = outSideSouthWestPoint.lng - inSideSouthWestPoint.lng;
	                		var newCenter = new BMap.Point(center.lng + deltaLng, center.lat + deltaLat);
	                		map.setCenter(newCenter);
	                	}
	                } else {
		                self.you = new BMap.Marker(point);
		                map.addOverlay(self.you);
		                var label = new BMap.Label("YOU", {offset:new BMap.Size(20,-10)});
		                self.you.setLabel(label);
		                map.centerAndZoom(point, 17);
	                } 
	            }, function(error){
	                console.log(error);
	            }, options);
	        }
	    }, 500);
    }

    drawBusRoute(){
    	var self = this;
    	var BMap = this.win.BMap;
    	var map = this.mapUtils.map;

		 var driving = new BMap.DrivingRoute(map, {
			renderOptions: {
				map: map,
				panel: "r-result",
				enableDragging : true //起终点可进行拖拽
			},  
		});
		driving.search("吉大总站","华发商都");
		driving.setSearchCompleteCallback(function(){
			var points = driving.getResults().getPlan(0).getRoute(0).getPath();
			driving.clearResults(); //清除地图上搜索的结果

			//画路线
			var polyline = new BMap.Polyline(points,{
				strokeColor: 'red',
				strokeWeight: 5,
				strokeOpacity: 0.8,
				strokeStyle: 'solid'
			});
			map.addOverlay(polyline);
		});
    }

    getBusRoute(){
    	var map = this.mapUtils.map;
    	var BMap = this.win.BMap;

    	this.http.get(this.appSetting.webApiServer + '/api/bus/route')
				.subscribe(data => {
					var busRoutes = data.json();
					console.log(busRoutes);
					if (busRoutes.length > 0){
						var route = busRoutes[0].path;
						if(route && route.length > 0){
							var points = new Array();
							for(var i=0; i < route.length; i++){
								points[i] = new BMap.Point(route[i].long, route[i].lat);
							}

							//画路线
							var polyline = new BMap.Polyline(points,{
								strokeColor: 'red',
								strokeWeight: 5,
								strokeOpacity: 0.8,
								strokeStyle: 'solid'
							});
							map.addOverlay(polyline);
							map.centerAndZoom(points[0], 17);
						}
					}
				}, error => {
					console.log(JSON.stringify(error.json()));
				});
    }
}
