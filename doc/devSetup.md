# How to setup dev environment for mobile App with Ionic.

## Start Ionic App
   * You can run Ionic app browser or in an emulator for Android or iPhone. 
   * Before you run Ionic app, you need to add the corresponding platform with `Ionic platform add platform_name`, where platform_name can be `browser, ios, android`.
   
   * run `npm install` to install nodejs packages needed by Ionic.
   * run `ionic run browser` to start Ionic App in a browser.

## How to debug
### On Mac or Windows:
   * Use Chrome developer tools to see console.log.
      * Only Chrome support FileSystem API. Cordova.plugin.file implements FileSystem API.

   * When running on Android simulator Genymotion:
      1. Use Chrome browser with location set to "chrome://inspect" to debug. 
      2. If cordova.plugin.file is used to read/write data to local file, install Chrome extension HTML5 FileSystem Explorer to check the files on PC. 
         The file is write to a location specific to each APP. And each APP is sandboxed.  Click on the HTML5 FileSystem Explorer to check files.
      3. In the location bar of Chrome browser, use filesystem:http://localhost:8000/persistent/ or filesystem:http://localhost:8000/temporary/
      4. Go to chrome://flags and enable "Enable Developer Tools experiments", then go to the Experiments tab in the Dev Tools settings,there's one named "FileSystem Inspection" that might work. I haven't tried it, though.



### On MAC only:
   * When running on iOS simulator comes with MAC
      1. In Safari Preference Advanced, check the "Show Develop Menu in the Menu bar". Then in Safari -> Develop -> Simulator, you can have a debug window similar to the Chrome Developer Tool.
      2. If cordova.plugin.file is used, open Finder and go to the dir `~/Developer/CoreSimulator/Devices/{DeviceID}/data/Container/Data/Application/{AppID}` to check the file. Use `xcrun simctl --list` to get the DeviceID. Do not know how to get AppID.

### When running on iOS devices:
   1. In Safari Preference Advanced, check the "Show Develop Menu in the Menu bar".
   2. On your iPad, iPhone or iPod touch, tap Settings > Safari > Advanced and toggle on Web Inspector.
      

## Apple Provisioning Profile
A provisioning profile defines who can code sign an APP and which devices can run the App. Using XCode to automatically manage a team provisioning profile is easy and best practice.
You can manually define a provisioning profile to restrict who can sign and which devices can test. A device need to register in Apple Developer Center in order to do testing.
  

